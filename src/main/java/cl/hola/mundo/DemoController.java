package cl.hola.mundo;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class DemoController {


    @GetMapping
    public String getHello(){
        return "Hello World";
    }
}
